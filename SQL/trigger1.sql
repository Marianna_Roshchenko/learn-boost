-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
IF OBJECT_ID ('[les_stud_vtrigger]','TR') IS NOT NULL
    DROP TRIGGER [dbo].[les_stud_vtrigger];
GO
-- =============================================
CREATE TRIGGER les_stud_vtrigger
ON [dbo].les_stud
FOR INSERT, UPDATE, DELETE 
AS
   BEGIN
   begin
     if (select count(*) from dbo.data_versions where name = 'les_stud')<1
       insert into dbo.data_versions(name,value) values ('les_stud',0);
   end
    update dbo.data_versions set value = value + 1 where name = 'les_stud';
   END
GO

-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
IF OBJECT_ID ('[students_vtrigger]','TR') IS NOT NULL
    DROP TRIGGER [dbo].students_vtrigger;
GO
-- =============================================
CREATE TRIGGER students_vtrigger
ON [dbo].students
AFTER INSERT, UPDATE, DELETE 
AS
   BEGIN
   begin
     if (select count(*) from dbo.data_versions where name = 'students')<1
       insert into dbo.data_versions(name,value) values ('students',0);
   end
    update dbo.data_versions set value = value + 1 where name = 'students';
   END
GO

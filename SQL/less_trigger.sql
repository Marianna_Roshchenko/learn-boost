-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
IF OBJECT_ID ('[lessons_vtrigger]','TR') IS NOT NULL
    DROP TRIGGER [dbo].lessons_vtrigger;
GO
-- =============================================
CREATE TRIGGER lessons_vtrigger
ON [dbo].lessons
AFTER INSERT, UPDATE, DELETE 
AS
   BEGIN
   begin
     if (select count(*) from dbo.data_versions where name = 'lessons')<1
       insert into dbo.data_versions(name,value) values ('lessons',0);
   end
    update dbo.data_versions set value = value + 1 where name = 'lessons';
   END
GO

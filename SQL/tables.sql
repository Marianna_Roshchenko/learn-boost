USE [learnboost]
GO

/****** Object:  Table [dbo].[lessons]    Script Date: 20.12.2012 23:39:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[lessons](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_lessons] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/***********************************************************************/
USE [learnboost]
GO

/****** Object:  Table [dbo].[students]    Script Date: 20.12.2012 23:40:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[students](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[surname] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_students] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO





/***********************************************************************/

USE [learnboost]
GO

/****** Object:  Table [dbo].[les_stud]    Script Date: 20.12.2012 23:38:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[les_stud](
	[stud_id] [int] NOT NULL,
	[les_id] [int] NOT NULL,
	[grade] [int] NOT NULL,
 CONSTRAINT [PK_les_stud] PRIMARY KEY CLUSTERED 
(
	[stud_id] ASC,
	[les_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[les_stud]  WITH CHECK ADD  CONSTRAINT [FK_les_stud_lessons] FOREIGN KEY([les_id])
REFERENCES [dbo].[lessons] ([id])
GO

ALTER TABLE [dbo].[les_stud] CHECK CONSTRAINT [FK_les_stud_lessons]
GO

ALTER TABLE [dbo].[les_stud]  WITH CHECK ADD  CONSTRAINT [FK_les_stud_students] FOREIGN KEY([stud_id])
REFERENCES [dbo].[students] ([id])
GO

ALTER TABLE [dbo].[les_stud] CHECK CONSTRAINT [FK_les_stud_students]
GO




﻿using System;
using System.Data.Entity;
using System.Linq;
using Lab.Model;

namespace Lab.Data.Contracts.Impl
{
    public class StudentsRepository : EFRepository<Student>, IStudentsRepository
    {
        public StudentsRepository(DbContext context) : base(context) { }
        public System.Collections.Generic.ICollection<Student> getAllStudents()
        {
            return this.DbContext.Set<Student>().ToList<Student>();
        }


        public System.Collections.Generic.ICollection<Grade> GetGrades(int studID)
        {
            return this.DbContext.Set<Grade>().Where<Grade>(x => x.stud_id == studID).ToList<Grade>();
        }


        public void UpdateGrades(System.Collections.Generic.ICollection<Grade> grades)
        {
            foreach (var g in grades)
            {
                Grade grade = this.DbContext.Set<Grade>().Find(new object[g.les_id,g.stud_id]);
                this.DbContext.Set<Grade>().Remove(grade);
            }
            foreach (var g in grades)
            {
                this.DbContext.Set<Grade>().Add(g);
            }
        }
        
        public void UpdateGrade(Grade grade)
        {
            Grade g = null;
            if (this.DbContext.Set<Grade>() != null)
            {
                try
                {
                    g = this.DbContext.Set<Grade>().Single(x => x.les_id == grade.les_id && x.stud_id == grade.stud_id);
                }
                catch (Exception e) { 
                }
            }
                //Find(new object[grade.stud_id, grade.less_id]);
            try
            {
                if (g != null)
                    this.DbContext.Set<Grade>().Remove(g);
                this.DbContext.Set<Grade>().Add(grade);
            }
            catch (Exception e)
            {
            }
        }
    }
}

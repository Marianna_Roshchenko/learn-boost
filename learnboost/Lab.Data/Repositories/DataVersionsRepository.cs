﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Data.Contracts;
using Lab.Data.Contracts.Impl;
using Lab.Model;

namespace Lab.Data.Repositories
{
    public class DataVersionsRepository : EFRepository<DataVersion> , IDataVersionsRepository
    {
        public DataVersionsRepository(DbContext context) : base(context) { }
    }
}

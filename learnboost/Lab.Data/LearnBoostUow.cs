﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Data.Helpers;
using Lab.Model;

namespace Lab.Data.Contracts.Impl
{
    public class LearnBoostUow : ILearnBoostUow, IDisposable
    {
        private learnboostEntities DbContext { get; set; }
        protected IRepositoryProvider RepositoryProvider { get; set; }

        public LearnBoostUow(IRepositoryProvider repositoryProvider)
        {
            
            CreateDbContext();
            
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider; 
         
        }

        protected void CreateDbContext()
        {
            DbContext = new learnboostEntities();

            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }
        /// <summary>
        /// Save pending changes to the database
        /// </summary>
        public void Commit()
        {
            //System.Diagnostics.Debug.WriteLine("Committed");
            DbContext.SaveChanges();
        }

        public IStudentsRepository Students
        {
            get { return GetRepo<IStudentsRepository>(); }
        }

        public ILessonsRepository Lessons
        {
            get { return GetRepo<ILessonsRepository>(); }
        }

        public IDataVersionsRepository Versions
        {
            get { return GetRepo<IDataVersionsRepository>(); }
        }



        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion


       
    }
}

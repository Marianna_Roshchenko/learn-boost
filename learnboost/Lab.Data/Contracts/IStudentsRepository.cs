﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Model;

namespace Lab.Data.Contracts
{
    public interface IStudentsRepository : IRepository<Student>
    {
        ICollection<Student> getAllStudents();
        ICollection<Grade> GetGrades(int studID);
        void UpdateGrades(ICollection<Grade> grades);
        void UpdateGrade(Grade grade);
    }
}

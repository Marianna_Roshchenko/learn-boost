﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Model;

namespace Lab.Data.Contracts
{
    public interface ILearnBoostUow
    {
         void Commit();
         IStudentsRepository Students { get; }
         ILessonsRepository Lessons { get; }
         IDataVersionsRepository Versions { get; }
    }
}

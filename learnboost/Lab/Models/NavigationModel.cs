﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab.Models
{
    public class NavigationLink
    {
        public String ControllerName { get; set; }
        public String ActionName { get; set; }
        public String Name {get;set;}
        public Boolean Active { get; set; }

        public NavigationLink() { 
        
        }
        public NavigationLink(String ControllerName, String ActionName, String Name)
        {
            this.ControllerName = ControllerName;
            this.ActionName = ActionName;
            this.Name = Name;
            this.Active = false;
        }
        public NavigationLink(String ControllerName, String ActionName, String Name, Boolean Active)
        {
            this.ControllerName = ControllerName;
            this.ActionName = ActionName;
            this.Name = Name;
            this.Active = Active;
        }
    }

    public class NavigationModel
    {

      public  ICollection<NavigationLink> Links { get; set; }

      public NavigationModel(String activeLinkName) {
          Links = new List<NavigationLink>();
          Links.Add(new NavigationLink("Students", "Roster", "Administration" ));
          Links.Add(new NavigationLink("Grades", "Gradebook", "Gradebook"));
          
          foreach (var link in Links) {
              if (activeLinkName.Equals(link.Name)) {
                  link.Active = true; 
              }
          }
          
      }
    }
}
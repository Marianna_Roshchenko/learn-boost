﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab.Models
{
    public class VersionsModel
    {
        public int StudentsVersion { get; set; }
        public int LessonsVersion { get; set; }
        public int MarksVersion { get; set; }
        public VersionsModel()
        {
            StudentsVersion = 0;
            LessonsVersion = 0;
            MarksVersion = 0;
        }

        public VersionsModel Clone()
        {
            VersionsModel v = new VersionsModel();
            v.LessonsVersion = LessonsVersion;
            v.MarksVersion = MarksVersion;
            v.StudentsVersion = StudentsVersion;
            return v;
        }

        public void EncStudentsVersion()
        {
            StudentsVersion++;
        }
        public void EncLessonsVersion()
        {
            LessonsVersion++;
        }
        public void EncMarksVersion()
        {
            MarksVersion++;
        }
    }
}
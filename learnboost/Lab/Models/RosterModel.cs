﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab.Models
{
    public class RosterModel : LayoutModel
    {
        public ICollection<StudentModel> Students { get; set; }
        public VersionsModel DataVersions { get; set; }
    }
}
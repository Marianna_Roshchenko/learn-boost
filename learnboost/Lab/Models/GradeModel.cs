﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab.Model;

namespace Lab.Models
{
    public class GradeModel
    {
        public int StudentID { get; set; }
        public int LessonID { get; set; }
        public int Mark { get; set; }
        public GradeModel() { 
        
        }
        public GradeModel(Grade m)
        {
            StudentID = m.stud_id;
            LessonID = m.les_id;
            Mark = m.grade;

        }

        public Grade MakeGradeObj() {
            Grade g = new Grade();
            g.stud_id = StudentID;
            g.les_id = LessonID;
            g.grade = Mark;

            return g;
        }
    }
}
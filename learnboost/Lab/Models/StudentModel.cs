﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab.Model;

namespace Lab.Models
{
    public class StudentModel
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Email { get; set; }
        public String Phone { get; set; }

        public StudentModel(Student s) {
            this.ID = s.id;
            this.Name = s.name;
            this.Surname = s.surname;
            this.Phone = s.phone;
            this.Email = s.email;
            
        }
        public StudentModel() { 
        
        }
        public StudentModel(int id, String name, String surname, String phone, String email) {
            this.ID = id;
            this.Name = name;
            this.Surname = surname;
            this.Phone = phone;
            this.Email = email;
        }

        public Student makeStudentObj() {
            Student s = new Student();
            s.id = ID;
            s.name = Name;
            s.surname = Surname;
            s.phone = Phone;
            s.email = Email;

            return s;
        }
    }
}
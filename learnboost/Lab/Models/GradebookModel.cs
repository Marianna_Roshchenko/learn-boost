﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab.Models
{
    public class GradebookModel : LayoutModel
    {
        public VersionsModel DataVersions { get; set; }
        public ICollection<GradeStudentModel> Students { get; set; }
        public ICollection<LessonModel> Lessons { get; set; }
    }
}
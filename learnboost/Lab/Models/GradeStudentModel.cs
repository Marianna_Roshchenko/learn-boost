﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lab.Models
{
    public class GradeStudentModel
    {
        public StudentModel Student { get; set; }
        public ICollection<GradeModel> Grades { get; set; }

    }
}
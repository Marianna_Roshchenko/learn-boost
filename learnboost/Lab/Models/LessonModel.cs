﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab.Model;

namespace Lab.Models
{
    public class LessonModel
    {
        public int ID { get; set; }
        public String Name { get; set; }

        public LessonModel() { 
        
        }
        public LessonModel(Lesson less) {
            ID = less.id;
            Name = less.name;
        }

        public Lesson MakeLessonObj() {
            Lesson less = new Lesson();
            less.id = ID;
            less.name = Name;
            return less;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Mvc;
using Lab.Data.Contracts;
using Lab.Data.Contracts.Impl;
using Lab.Data.Helpers;
using Lab.Data.Helpers.Impl;
using Lab.Helpers;
using Lab.Model;
using Lab.Models;
using Lab.Extensions;
namespace Lab.Controllers
{
    public class StudentsController : BaseController
    {
        public StudentsController(ILearnBoostUow uow)
        {
            Uow = uow;
        }

        public StudentsController()
        {
          

        }

        public ActionResult Roster( )
        {
            RosterModel model = new RosterModel();
            model.DataVersions = VersionsModelHelper.Make(Uow);
            model.Navigation = new NavigationModel("Administration");
            model.Students = new List<StudentModel>();
            
            foreach (var s in Uow.Students.GetAll())
            {
                model.Students.Add(new StudentModel(s));
            }
            return View(model);
        }

   
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Remove(int id)
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                VersionsModel newVersions = curVersions.Clone();
                newVersions.EncStudentsVersion();
                Uow.Students.Delete(id);
                Uow.Commit();
                result.OldDataVersions = curVersions;
                result.NewDataVersions = newVersions;
                result.Success = true;
                
            }
            catch (Exception e) {
                result.Success = false;
            }

            return ExpandoJsonConverter.toActionResult(result);
        }

        [HttpPost]
        public ActionResult Insert(StudentModel student)
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                VersionsModel newVersions = curVersions.Clone();
                newVersions.EncStudentsVersion();

                Student st = student.makeStudentObj();
                Uow.Students.Add(st);
                Uow.Commit();
                result.Student = new StudentModel(st);
                result.OldDataVersions = curVersions;
                result.NewDataVersions = newVersions;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
            }
            return ExpandoJsonConverter.toActionResult(result);
        }

        [HttpPost]
        public ActionResult Update(StudentModel student)
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                VersionsModel newVersions = curVersions.Clone();
                newVersions.EncStudentsVersion();
                Student st = student.makeStudentObj();
                Uow.Students.Update(st);
                Uow.Commit();
                result.Student = new StudentModel(st);
                result.OldDataVersions = curVersions;
                result.NewDataVersions = newVersions;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
            }
            return ExpandoJsonConverter.toActionResult(result);
        }

        [HttpPost]
        public ActionResult GetAll()
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                var students = new List<StudentModel>();
                foreach (var s in Uow.Students.GetAll()) {
                    students.Add(new StudentModel(s));
                }
                result.OldDataVersions = curVersions;
                result.NewDataVersions = curVersions;
                result.Students = students;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
               
            }

            return ExpandoJsonConverter.toActionResult(result);
            
        }



    }
}
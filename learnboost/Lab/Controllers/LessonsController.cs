﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lab.Data.Contracts;
using Lab.Extensions;
using Lab.Helpers;
using Lab.Model;
using Lab.Models;

namespace Lab.Controllers
{
    public class LessonsController : BaseController
    {
        public LessonsController(ILearnBoostUow uow)
        {
            Uow = uow;
        }

        public LessonsController()
        {
          

        }

        [HttpPost]
        public ActionResult Insert(LessonModel lesson)
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                VersionsModel newVersions = curVersions.Clone();
                newVersions.EncLessonsVersion();

                Lesson less = lesson.MakeLessonObj();
                less.id = 0;
                Uow.Lessons.Add(less);
                Uow.Commit();
                result.Lesson = new LessonModel(less);
                result.OldDataVersions = curVersions;
                result.NewDataVersions = newVersions;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
                throw new Exception("", e);
            }
            return ExpandoJsonConverter.toActionResult(result);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            dynamic result = new ExpandoObject();
            try
            {
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                VersionsModel newVersions = curVersions.Clone();
                newVersions.EncLessonsVersion();
                Uow.Lessons.Delete(id);
                Uow.Commit();
                result.OldDataVersions = curVersions;
                result.NewDataVersions = newVersions;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
                throw new Exception("", e);
            }
            return ExpandoJsonConverter.toActionResult(result);
        }


    }
}

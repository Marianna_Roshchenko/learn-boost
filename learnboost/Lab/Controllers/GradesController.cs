﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Lab.Data.Contracts;
using Lab.Extensions;
using Lab.Helpers;
using Lab.Models;

namespace Lab.Controllers
{
    public class GradesController : BaseController
    {
        public GradesController(ILearnBoostUow uow)
        {
            Uow = uow;
        }

        public GradesController()
        {


        }

        public ActionResult Gradebook()
        {
            GradebookModel model = new GradebookModel();
            var gradeStudents = new List<GradeStudentModel>();
            model.Navigation = new NavigationModel("Gradebook");
            model.Lessons = new List<LessonModel>();
            model.Students = new List<GradeStudentModel>();
            model.DataVersions = VersionsModelHelper.Make(Uow);
            foreach (var less in Uow.Lessons.GetAll())
            {
                model.Lessons.Add(new LessonModel(less));
            }
            foreach (var s in Uow.Students.getAllStudents())
            {
                GradeStudentModel sm = new GradeStudentModel();
                sm.Student = new StudentModel(s);
                sm.Grades = new List<GradeModel>();
                foreach (var m in Uow.Students.GetGrades(s.id))
                {
                    sm.Grades.Add(new GradeModel(m));
                }
                model.Students.Add(sm);

            }
            return View(model);
        }

        public ActionResult getAllGrades()
        {
            dynamic result = new ExpandoObject();
            try
            {
                var Lessons = new List<LessonModel>();
                var Students = new List<GradeStudentModel>();
                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                foreach (var less in Uow.Lessons.GetAll())
                {
                    Lessons.Add(new LessonModel(less));
                }
                foreach (var s in Uow.Students.getAllStudents())
                {
                    GradeStudentModel sm = new GradeStudentModel();
                    sm.Student = new StudentModel(s);
                    sm.Grades = new List<GradeModel>();
                    foreach (var m in Uow.Students.GetGrades(s.id))
                    {
                        sm.Grades.Add(new GradeModel(m));
                    }
                    Students.Add(sm);

                }
                result.OldDataVersions = curVersions;
                result.NewDataVersions = VersionsModelHelper.Make(Uow);
                result.Lessons = Lessons;
                result.Students = Students;
                result.Success = true;
            }
            catch (Exception e)
            {
                result.Success = false;
                throw new Exception("", e);
            }
            return ExpandoJsonConverter.toActionResult(result);
        }

        [HttpPost]
        public ActionResult SaveGrades(GradeModel[] grades)
        {
            dynamic result = new ExpandoObject();
            result.Success = true;
            try
            {
                if (grades != null)
                {
                    foreach (var g in grades)
                    {
                        Uow.Students.UpdateGrade(g.MakeGradeObj());
                    }
                    Uow.Commit();
                }
            }
            catch (Exception e)
            {
                result.Success = false;
            }

            try
            {

                VersionsModel curVersions = VersionsModelHelper.Make(Uow);
                result.OldDataVersions = curVersions;
                result.NewDataVersions = VersionsModelHelper.Make(Uow);
            }
            catch (Exception e)
            {
                result.Success = false;
            }

            return ExpandoJsonConverter.toActionResult(result);
        }

    }
}
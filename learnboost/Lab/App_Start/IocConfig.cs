﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Lab.Data.Contracts;
using Lab.Data.Contracts.Impl;
using Lab.Data.Helpers;
using Lab.Data.Helpers.Impl;
using Ninject;
using Ninject.Web.Mvc;
using NinjectAdapter;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Lab.App_Start.IocConfig), "RegisterIoc")]
namespace Lab.App_Start
{
    public static class IocConfig
    {
        public static void RegisterIoc()
        {
            var kernel = new StandardKernel(); // Ninject IoC

            // These registrations are "per instance request".
            // See http://blog.bobcravens.com/2010/03/ninject-life-cycle-management-or-scoping/

            kernel.Bind<RepositoryFactories>().To<RepositoryFactories>()
                .InSingletonScope();
            kernel.Bind<IRepositoryProvider>().To<RepositoryProvider>();
            kernel.Bind<ILearnBoostUow>().To<LearnBoostUow>();

            // Tell WebApi how to use our Ninject IoC
            DependencyResolver.SetResolver(new NinjectServiceLocator(kernel));
      
           // config.DependencyResolver = new NinjectDependencyResolver(kernel);
        }
    }
}
﻿

var NetHelper = function () {
    var self = this;

    self.insertLesson = function (lesson, call_func) {
        console.log(lesson);
        $.ajax({
            url: "/Lessons/Insert",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(lesson),
            success: function (data) {

                var result = {
                    oldVers : (data ? data.OldDataVersions : null),
                    newVers : (data ? data.NewDataVersions : null),
                    lesson  : (data ? data.Lesson : null),
                    success : (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });

    }

    self.removeLesson = function (lesson_id, call_func) {
        var params = { id: lesson_id };

        $.ajax({
            url: "/Lessons/Remove",
            type: "POST",
            data: JSON.stringify(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });
    }

    self.saveGrades = function (grades, call_func) {
        
        $.ajax({
            url: "/Grades/SaveGrades",
            type: "POST",
            data: JSON.stringify(grades),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });
    }
    self.getAllGrades = function (call_func) {
        $.ajax({
            url: "/Grades/getAllGrades",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    studs: (data ? data.Students : null),
                    lessons: (data ? data.Lessons : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });

    }

    self.insertStudent = function (stud, call_func) {
        $.ajax({
            url: "/Students/Insert",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(stud),
            success: function (data) {
               
                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    student : (data ? data.Student : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });

    }

    self.updateStudent = function (stud, call_func) {
        var params = { student: stud };
        console.log(params);
        $.ajax({
            url: "/Students/Update",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(stud),
            success: function (data) {

                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    student: (data ? data.Student : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });

    }

    self.removeStudent = function (stud_id, call_func) {
        var params = { id: stud_id };

        $.ajax({
            url: "/Students/Remove",
            type: "POST",
            data: JSON.stringify(params),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    success: false
                }
                call_func(result);
            }
        });
    }


    self.getAllStudents = function ( call_func) {

        $.ajax({
            url: "/Students/GetAll",
            type: "POST",
            dataType: "json",
            success: function (data) {

                var result = {
                    oldVers: (data ? data.OldDataVersions : null),
                    newVers: (data ? data.NewDataVersions : null),
                    students: (data ? data.Students : null),
                    success: (data ? data.Success : false)
                }
                call_func(result);
            },
            error: function () {
                var result = {
                    oldVers: null,
                    newVers: null,
                    students : null,
                    success: false
                }
                call_func(result);
            }
        });

    }
}

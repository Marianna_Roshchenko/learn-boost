/*global ko, crossroads */
(function () {
    'use strict';
    
    var Lesson = function (id, name) {
        var self = this;
        self.id = ko.observable(id);
        self.name = ko.observable(name);
    };

    var Grade = function (lessonID, value) {
        var self = this;
        this.value = ko.observable(value);
        this.lessonID = ko.observable(lessonID);
    };
    var GradeHelper = function () {
        this.getGradeByLesson = function (grades, lesonID) {
            for (var i = 0; i < grades.length ; i++) {
                if (grades[i].LessonID == lesonID)
                    return new Grade(grades[i].LessonID, grades[i].Mark);
            }
            return new Grade(lesonID, 0);
        }

    };
    // represent a single todo item
    var Stud = function (id,firstName, secondName, grades) {
        var self = this;
        self.id = ko.observable(id);
        self.firstName = ko.observable(firstName);
        self.secondName = ko.observable(secondName);
        self.grades = ko.observableArray(grades);

        self.average = ko.computed(function () {
           
            var symbolMark = "F";
            var grades = ko.toJS(self.grades);
            if (grades.length == 0)
                return symbolMark;
            var avr = 0;
            for (var i = 0; i < grades.length; i++) {
                avr += 1 * grades[i].value;
            }
            var avrMark = avr / grades.length;
            if (avrMark > 4.5)
                symbolMark = "A";
            else
                if (avrMark >= 4.2)
                    symbolMark = "B";
                else
                    if (avrMark > 3.8)
                        symbolMark = "C";
                    else
                        if (avrMark > 3.4)
                            symbolMark = "D";
                        else if (avrMark > 2.9)
                            symbolMark = "E";

            return symbolMark;
        });
    };

    // our main view model
    var ViewModel = function (studs, lessons, vers) {
        var self = this;

        self.vers = vers;

        self.lessons = ko.observableArray(ko.utils.arrayMap(lessons, function (lesson) {
            return new Lesson(lesson.id,lesson.name);
        }));

        // map array of passed in todos to an observableArray of Todo objects
        self.studs = ko.observableArray(ko.utils.arrayMap(studs, function (stud) {
            return new Stud(stud.id,stud.firstName, stud.secondName, stud.grades);
        }));

        self.lessonToAdd = ko.observable("");

        self.setDataModel = function (Model) {
            var lessons = [];
            for (var i = 0; i < Model.lessons.length; i++) {
                lessons.push(new Lesson(Model.lessons[i].ID, Model.lessons[i].Name));
            }
            self.lessons.removeAll();
            ko.utils.arrayForEach(lessons, function (lesson) {
                self.lessons.push(new Lesson(lesson.id, lesson.name));
            });

            var gradeHelper = new GradeHelper();
            for (var j = 0; j < Model.studs.length; j++) {
                Model.studs[j].grades = [];
            }
            for (var i = 0; i < Model.lessons.length; i++) {
                for (var j = 0; j < Model.studs.length; j++) {
                    Model.studs[j].grades.push(gradeHelper.getGradeByLesson(Model.studs[j].Grades, Model.lessons[i].ID));
                }
            }
            console.log(Model);
            var students = [];
            for (var i = 0; i < Model.studs.length; i++) {
                var stud = new Stud(Model.studs[i].Student.ID,
                                    Model.studs[i].Student.Name,
                                    Model.studs[i].Student.Surname, 
                                    Model.studs[i].grades);
                console.log(Model.studs[i].grades);
                students.push(stud);
            }
          
            ko.utils.arrayForEach(self.studs, function (stud) {
                stud.grades.removeAll();
            });

            self.studs.removeAll();
            ko.utils.arrayForEach(students, function (stud) {
                self.studs.push(new Stud(stud.id, stud.firstName, stud.secondName, stud.grades));
            });
            
        }


        self.addLesson = function () {
            var lessonID = -1;
            var lessonName = self.lessonToAdd();
            if (lessonName != "") {
                var requestData = {
                    ID: lessonID,
                    Name: lessonName
                };
                var net = new NetHelper();

                net.insertLesson(requestData, function (result) {
                    if (result.success == true) {
                        self.lessons.push(new Lesson(result.lesson.ID, lessonName));
                        ko.utils.arrayForEach(self.studs(), function (stud) {
                            stud.grades.push(new Grade(result.lesson.ID, 0));
                        });
                        self.synchronize(result.oldVers, result.newVers);
                    }
                });


            }
        }

        self.removeLesson = function (lesson) {
            var lessonObj = ko.toJS(lesson);
           
            if (lessonObj.id == -1) {
                return;
            }

            var requestData = lessonObj.id;
            var net = new NetHelper();

            net.removeLesson(requestData, function (result) {
                if (result.success == true) {
                    self.lessons.remove(lesson);
                    ko.utils.arrayForEach(self.studs(), function (stud) {
                        stud.grades().destroy(function (grade) {
                            if (ko.toJS(grade).lessonID == lessonObj.id)
                                return true;
                            return false;
                        });
                    });
                    self.synchronize(result.oldVers, result.newVers);
                }
            });
           
        }

        self.save = function () {
            var net = new NetHelper();
            var requestData = [];
            ko.utils.arrayForEach(self.studs(), function (stud) {
                var studObj = ko.toJS(stud);
                var grades = studObj.grades;
                for (var i = 0; i < grades.length; i++) {
                    requestData.push({
                        StudentID: studObj.id,
                        LessonID : grades[i].lessonID,
                        Mark     : grades[i].value
                    });
                }
               
            });
            net.saveGrades(requestData, function (result) {
                if (result.success == true) {
                    
                    console.log(result);
                }
                self.synchronize(result.oldVers, result.newVers);
            });

        }

        self.refresh = function () {
            var net = new NetHelper();
            net.getAllGrades(function (result) {
                if (result.success == true) {
                    self.vers = result.newVers;
                    console.log(result);
                    self.setDataModel(result);
                }

            });

        }


        self.synchronize = function (oldServerVers, newServerVers) {
            console.log(self.vers);
            console.log(oldServerVers);
            if (oldServerVers.StudentsVersion <= self.vers.StudentsVersion &&
                oldServerVers.LessonsVersion <= self.vers.LessonsVersion &&
                oldServerVers.MarksVersion <= self.MarksVersion) {
                self.vers = newServerVers;
                return;
            }
            self.refresh();

        }
    };

    var viewModel = new ViewModel([], [], Model.vers);
    viewModel.setDataModel(Model);
    ko.applyBindings(viewModel);

    // set up filter routing
}());
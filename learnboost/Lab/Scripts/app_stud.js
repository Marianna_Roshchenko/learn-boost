/*global ko, crossroads */
(function () {
    'use strict';

    var ENTER_KEY = 13;
    // a custom binding to handle the enter key (could go in a separate library)
    ko.bindingHandlers.enterKey = {
        init: function (element, valueAccessor, allBindingsAccessor, data) {
            var wrappedHandler, newValueAccessor;

            // wrap the handler with a check for the enter key
            wrappedHandler = function (data, event) {
                valueAccessor().call(this, data, event);
                if (event.keyCode === ENTER_KEY) {
                    valueAccessor().call(this, data, event);
                }
            };

            // create a valueAccessor with the options that we would want to pass to the event binding
            newValueAccessor = function () {
                return {
                    keyup: wrappedHandler
                };
            };

            // call the real event binding's init function
            ko.bindingHandlers.event.init(element, newValueAccessor, allBindingsAccessor, data);
        }
    };

    ko.bindingHandlers.touchKey = {
        init: function (element, valueAccessor, allBindingsAccessor, data) {
            var wrappedHandler, newValueAccessor;

            // wrap the handler with a check for the enter key
            wrappedHandler = function (data, event) {
                if (event.keyCode !== ENTER_KEY) {
                    valueAccessor().call(this, data, event);
                }
            };

            // create a valueAccessor with the options that we would want to pass to the event binding
            newValueAccessor = function () {
                return {
                    keyup: wrappedHandler
                };
            };

            // call the real event binding's init function
            ko.bindingHandlers.event.init(element, newValueAccessor, allBindingsAccessor, data);
        }
    };

    var Stud = function (id, firstName, secondName, email, phone, showSave) {
        var self = this;
        self.id = ko.observable(id);
        self.firstName = ko.observable(firstName);
        self.secondName = ko.observable(secondName);
        self.email = ko.observable(email);
        self.phone = ko.observable(phone);
        self.isShowSave = ko.observable(showSave);
    };

    // our main view model
    var ViewModel = function (studs, vers) {
        var self = this;
        var count = 3;
        self.vers = vers;

        self.isSelected = ko.observable(false);
        self.setUnSelected = function () {
            self.isSelected(false);
        }
        self.current = ko.observable();

        // map array of passed in todos to an observableArray of Todo objects
        self.studs = ko.observableArray(ko.utils.arrayMap(studs, function (stud) {
            return new Stud(stud.id, stud.firstName, stud.secondName, stud.email, stud.phone,false);
        }));

        self.add = function () {
            var stud = new Stud(-1, "", "", "", "", true);
            self.studs.push(new Stud(stud.id, stud.firstName, stud.secondName, stud.email, stud.phone, stud.isShowSave));
        }

        self.remove = function (stud) {
            var studObj = ko.toJS(stud);
            console.log(studObj);
            if (studObj.id == -1) {
                self.studs.remove(stud);
                return;
            }
            var requestData = studObj.id;
            var net = new NetHelper();

            net.removeStudent(requestData, function (result) {
                if (result.success == true) {
                    self.studs.remove(stud);
                    self.synchronize(result.oldVers, result.newVers);
                }
            });

        }
       

        self.edit = function (stud) {
            stud.isShowSave(true);
        }

        self.save = function (stud) {
            var studObj = ko.toJS(stud);
            if (studObj.firstName == "") {
                return;
            }
            
            var requestData = {
                ID: studObj.id,
                Name: studObj.firstName,
                Surname: studObj.secondName,
                Email: studObj.email,
                Phone: studObj.phone
            };
            var net = new NetHelper();
            if (studObj.id == -1) {
                net.insertStudent(requestData, function (result) {
                    if (result.success == true) {
                        stud.id = ko.observable(result.student.ID);
                        stud.isShowSave(false);
                        self.synchronize(result.oldVers, result.newVers);
                    }
                });
            } else {
                net.updateStudent(requestData, function (result) {
                    if (result.success == true) {
                        stud.isShowSave(false);
                        self.synchronize(result.oldVers, result.newVers);
                    }
                });
            }
        }
        self.refresh = function () {
            var net = new NetHelper();
            net.getAllStudents(function (result) {
                if (result.success == true) {
                    self.vers = result.newVers;
                    var StudsModel = [];
                    for (var i = 0; i < result.students.length; i++) {
                        StudsModel.push(new Stud(result.students[i].ID,
                                                 result.students[i].Name,
                                                 result.students[i].Surname,
                                                 result.students[i].Email,
                                                 result.students[i].Phone,
                                                  false));
                    }
                    self.studs.removeAll();
                    ko.observableArray(ko.utils.arrayMap(StudsModel, function (stud) {
                        self.studs.push(
                            new Stud(stud.id, stud.firstName, stud.secondName, stud.email, stud.phone, false));
                    }));

                }

            });

        }
        self.synchronize = function (oldServerVers, newServerVers) {
            console.log(self.vers);
            console.log(oldServerVers);
            if (oldServerVers.StudentsVersion <= self.vers.StudentsVersion) {
                self.vers = newServerVers;
                return;
            }
            self.refresh();
      
        }
    };

    console.log(Model);

    var StudsModel = [];
    for (var i = 0; i < Model.studs.length; i++) {
        StudsModel.push(new Stud(Model.studs[i].ID,
                                 Model.studs[i].Name,
                                 Model.studs[i].Surname,
                                 Model.studs[i].Email,
                                 Model.studs[i].Phone,
                        false));
    }
    var viewModel = new ViewModel(StudsModel, Model.vers);
    ko.applyBindings(viewModel);

    // set up filter routing
}());
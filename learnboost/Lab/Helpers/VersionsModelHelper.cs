﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lab.Data.Contracts;
using Lab.Models;

namespace Lab.Helpers
{
    public static class VersionsModelHelper
    {
       public static VersionsModel Make(ILearnBoostUow uow)
        {
            if (uow == null)
                return new VersionsModel();
            var model = new VersionsModel();
            foreach (var v in uow.Versions.GetAll())
            {
                if (v.name == "students")
                    model.StudentsVersion = v.value;
                if (v.name == "lessons")
                    model.LessonsVersion = v.value;
                if (v.name == "les_stud")
                    model.MarksVersion = v.value;
            }
            return model;
        }
    }
}